<?php
namespace Home\Controller;
use Think\Controller;
use Logic\IndexLogic;
class IndexController extends Controller {
    function _initialize(){

        header("Content-Type: application/json");
        Vendor("oauth.ThinkOAuth2");
        $this -> lan = getLanguage();
        $this ->IndexLogic = new IndexLogic();
        $this ->keyword = "联合办公 移动式办公 服务式办公 共享办公 工位租赁 办公租赁 办公位出 创业办公室租赁 写字楼 写字楼出租 精装修办公租赁 小型办公室出租 小办公室租赁 独立办工位出租 创业办公 新天地
                           外滩商圈 核心CBD 市中心 徐汇区 静安区 黄浦区 长宁区 淮海中路 江西中路 南京西路 威海路 制造局路 金钟路";
        $this ->description = "米域MIXPACE，城市新空间运营商，用设计、智能、服务为新一代人提供完美、舒适和便利的办公和生活空间解决方案。在这里，打开你的未来的边界！";
    }
//    public function index(){//首页
//        $this->assign("keyword",$this -> keyword);
//        if($this -> lan=='en'){
//            $this->display('Index/index_en');
//        }else{
//            $this->display();
//        }
//    }
    public function space(){//活动页面
        redirect('http://www.mixpace.com/');
        //$this->display('index');
//        $this->assign("keyword",$this -> keyword);
//        if($this -> lan=='en'){
//            $this->display('index');
//        }else{
//            $this->display('index');
//        }
    }
    public function activity(){//活动页面
        //空间列表
        $this->assign("keyword",$this -> keyword);
        $params['lan'] = $this ->lan;
        $spacelist = json_decode($this ->IndexLogic->getSpaceList($params),true);
        $this->assign("spacelist",$spacelist);
        if($this -> lan=='en'){
            $this->display('Index/activity_en');
        }else{
            $this->display();
        }
    }
    //test
    public function test(){//活动页面
        $params['lan'] = 'en';
        echo $this ->IndexLogic->getSpaceList($params);die;
        echo getLanguage();
        echo 'test';
    }
    //新的landpage
    public function breathing(){//新的页面
        $this->assign("keyword",$this -> keyword);
        $params['lan'] = $this ->lan;
        $spacelist = json_decode($this ->IndexLogic->getSpaceList($params),true);
        $this->assign("spacelist",$spacelist);
        if($this -> lan=='en'){
            $this->display('Index/activity_en');
        }else{
            $this->display('Index/activity');
        }
    }
    //Amyitis 飞元开业活动页面
    //新的landpage
    public function open(){//新的页面
        $this->assign("keyword",$this -> keyword);
        $params['lan'] = $this ->lan;
        $spacelist = json_decode($this ->IndexLogic->getSpaceList($params),true);
        $this->assign("spacelist",$spacelist);
        $this->display('Index/amyitis_open');
    }
    public function opening(){
        $this->assign("keyword",$this -> keyword);
        $params['lan'] = $this ->lan;
        //$spacelist = json_decode($this ->IndexLogic->getSpaceList($params),true);
        //$this->assign("spacelist",$spacelist);
        $openings = [
            1=>['img'=>'http://ooc90m1fd.bkt.clouddn.com/opening-01.png','time'=>'18:30'],
            2=>['img'=>'http://ooc90m1fd.bkt.clouddn.com/opening-02.png','time'=>'18:30'],
            3=>['img'=>'http://ooc90m1fd.bkt.clouddn.com/opening-03.png','time'=>'18:30'],
            4=>['img'=>'http://ooc90m1fd.bkt.clouddn.com/opening-04.png','time'=>'18:30'],
            5=>['img'=>'http://ooc90m1fd.bkt.clouddn.com/opening-05.png','time'=>'18:30']
        ];

        //微信分享
//        vendor('WX.JSSDK');
//        $jssdk = new \JSSDK("wxd08c332d57714f0c", "13512eaf03220434895e1b5ec74f238e");
//        $signPackage = $jssdk->GetSignPackage();
        $hostUrl = C('APIURL').'index.php/api/com/getWxShareParam';
        $signPackage = curl_post($hostUrl,$params,'');
        $signPackage = json_decode($signPackage,true);
        //var_dump($signPackage);
        $this->assign('signPackage',$signPackage);
        //
        $this->assign('openings',$openings);

        $this->display('Index/opening');
    }
    /*
     *新增信息页 百度 seo 需要
     * */
    public function newslandpage(){
        $this->display();
    }

}