<?php

/*
 *  Copyright (c) 2015 The UCLBRT project authors. All Rights Reserved.
 *
 *  Use of this source code is governed by a Guangzhou Changlian Information Technology Co.,Ltd license
 *  that can be found in the LICENSE file in the root of the web site.
 *
 *   http://qrm.uclbrt.com
 *
 *  An additional intellectual property rights grant can be found
 *  in the file PATENTS.  All contributing project authors may
 *  be found in the AUTHORS file in the root of the source tree.
 */
namespace Logic;

class IndexLogic {

    function __construct() {
        header("Content-Type: application/json");
    }


    /***
     *空间列表
     */
    public function getSpaceList($params){
        $apiUrl = C('APIURL').'index.php/api/space/sitemainpage/lan/'.$params['lan'].'/';
        $headers[]="Content-Type: application/json";
        $headers[]="Accept-Charset: utf-8";
        $res = curl_get($apiUrl,'',$headers);
        $rs = json_decode($res,true);
        return json_encode($rs['result'][0],JSON_UNESCAPED_SLASHES);//不转义;
    }


}
