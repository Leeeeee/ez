<?php
return array(
	//'配置项'=>'配置值'
    'URL_MODEL'             =>  1,
    'DEFAULT_FILTER'        => 'htmlspecialchars,addslashes',
    'MODULE_DENY_LIST'      =>  array('Common','Runtime','Service'),
    'TMPL_TEMPLATE_SUFFIX'  =>  '.html',
    'TMPL_PARSE_STRING' =>array(
        '__UPLOAD__' => '/Public',
        '__STATIC__' => '/Public',
        '__STATIC_JS__' => '/Public/Js',
        '__STATIC_CSS__' => '/Public/Css',
        '__STATIC_IMG__' => '/Public/Images',
    ),
    /* 系统缓存 */
    'DATA_CACHE_TYPE'                   => 'Redis',
    'REDIS_HOST'                        => '127.0.0.1',
    //'REDIS_HOST'                        => '121.41.18.76',
    'REDIS_PORT'                        => 6379,
    'DATA_CACHE_TIME'                   => 3600,
    //API
    'APIURL' => 'http://mixapi.com/index.php/api/space/',
    'V'=>'20170412001',//js css 版本
);