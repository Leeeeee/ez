<?php if (!defined('THINK_PATH')) exit();?><html lang="en">

<head>
    <title>上海亦州建筑装饰</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <link rel="shortcut icon" href="/favicon.ico">
    <link href="/Dist/Css/styles.71dbfb053af866861e18.css" rel="stylesheet">
</head>

<body>
    <section id="app">
        <app></app>
    </section>
    <!-- 高德CDN -->
    <script src="/Dist/Js/app.min.71dbfb053af866861e18.js"></script>
</body>

</html>