import '../Css/animate';
import '../Css/swiper.min';
import '../Css/normalize';
import '../Css/common';
import 'babel-polyfill';
import Vue from 'vue';
import router from './router';
import _ from 'lodash';
import 'Public/Js/http.js';
import lang from 'Public/Js/lang.js';
import VueAwesomeSwiper from 'vue-awesome-swiper';
import VueCookie from 'vue-cookie';
import VueI18n from 'vue-i18n';
import app from 'Components/app';

Vue.use(VueAwesomeSwiper);
Vue.use(VueCookie);
Vue.use(VueI18n);

const getBrowserLanguage = () => {
    let browserLanguage;
    if (navigator.browserlanguage) {
        browserLanguage = navigator.browserlanguage == "zh-cn" ? 'zh' : 'en';
        return browserLanguage;
    } else {
        browserLanguage = navigator.language == "zh-CN" ? 'zh' : 'en';
        return browserLanguage;
    }
}
const i18n = new VueI18n({
    locale: getBrowserLanguage(), // 默认值
    messages: lang,
});

const vm = new Vue({
    el: "#app",
    data: {},
    mounted() {
        const docEl = document.documentElement,
            resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
            recalc = () => {
                var clientWidth = docEl.clientWidth > 768 ? 768 : docEl.clientWidth;
                if (!clientWidth) return;
                docEl.style.fontSize = 40 * (clientWidth / 375) + 'px';
            };
        if (!document.addEventListener) return;
        window.addEventListener(resizeEvt, recalc, false);
        document.addEventListener('DOMContentLoaded', recalc, false);
    },
    router,
    i18n,
    components: { app }
});
router.beforeEach((to, from, next) => {
    document.body.scrollTop = document.documentElement.scrollTop = 0;
    next();
});