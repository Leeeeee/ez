import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import { bus } from 'Public/Js/bus.js';
Vue.use(VueAxios, axios);

axios.defaults.baseURL = process.env.NODE_ENV ? 'http://api.mixpace.com' : 'http://114.55.255.164:8093';
//axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';

//添加请求拦截器
axios.interceptors.request.use(config => {
    //在发送请求之前做某事，比如说 设置loading动画显示
    bus.$emit('requesting');
    //console.log('requesting loading')
    return config
}, error => {
    //请求错误时做些事

    //bus.$emit('requestError');
    //console.log('requestError loading')
    return Promise.reject(error)
})

//添加响应拦截器
axios.interceptors.response.use(response => {
    //对响应数据做些事，比如说把loading动画关掉
    bus.$emit('requestFinish');
    //console.log('response loading')
    return response
}, error => {
    //请求错误时做些事
    console.log(error);
    bus.$emit('requestError');
    //bus.$emit('responseError');
    //console.log('responseError loading')
    return Promise.reject(error)
})