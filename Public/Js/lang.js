module.exports = {
    zh: {
        nav: {
            about: '关于我们',
            cases: '项目案例',
            team: '团队成员',
            contact: '联系我们',
            home:'首页'
        },
        homeSwiperInindex: {
            missionTitle: "我们的使命",
            missionContenta: '提供创新思维',
            missionContentb: '为客户创造一个健康，美好，高效的环境'
        },
        projects: {
            typeList: [{ type: 1, name: '办公空间设计' }, { type: 2, name: '医疗空间设计' }, { type: 3, name: '教育空间设计' }],
            moreProjectCase: '更多案例'
        },
        speciality: {
            title: '专长领域',
            list:[
                {
                    title:'项目投资与策划',
                    content:'项目开展前期，帮助客户做市场调研及分析, 项目定位，投资预算，避免盲目投资，节约资源。'
                },{
                    title:'设计和建筑',
                    content:'一站式服务理念, 将设计和施工融为一个不可分割的整体.'
                },{
                    title:'施工与施工管理',
                    content:'全方位的技术服务、行政管理服务及施工服务。从工程招标到工程结束，提供高效、高质的策划、管理服务。'
                },{
                    title:'项目运营与管理',
                    content:'为项目整个实施过程包括设计、采购及施工提供综合协调管理。'
                }
            ]
        },
        partner: {
            title: '我们的客户'
        },
        contact: {
            company: '上海亦州建筑装饰工程有限公司',
            title: '联系我们',
            address: '地址',
            addressValue: '上海市徐汇区虹漕路461号52栋4C单元',
            post: '邮编',
            tel: '电话',
            mail: '邮箱'
        },
        team: {
            title: '团队成员',
            photoType1: '项目会议',
            photoType2: '方案讨论',

        },
        about:{
            content:'上海亦州建筑装饰工程有限公司专注于为知名企业提供建筑及装饰设计、展览及展示设计、项目策划与管理，装饰及机电施工等专业服务，可为客户提供一站式服务，包括项目选址、可行性研究、设计、项目实施、项目移交给设施管理部门等。在整个工程实施过程中，不仅为客户提供高质量的设计施工服务，并为客户提供独一无二的项目建议，保证项目获得成功。'
        }
    },
    en: {
        nav: {
            about: 'About Us',
            cases: 'Project Cases',
            team: 'Team Members',
            contact: 'Contact Us',
            home:'Home Page'
        },
        homeSwiperInindex: {
            missionTitle: "Our Mission",
            missionContenta: 'To Provide Innovative Ideas',
            missionContentb: 'To Create A Healthy, Beautiful And Efficient Environment'
        },
        projects: {
            typeList: [{ type: 1, name: 'Work Space Design' }, { type: 2, name: 'Medical Space Design' }, { type: 3, name: 'Educational Space Design' }],
            moreProjectCase: 'More Project Cases'
        },
        speciality: {
            title: 'Areas Of Expertise',
            list:[
                {
                    title:'Project Planning',
                    content:'Project pre-planning,  helping client to do market research and analysis, project positioning, investment budget, avoiding blind investment, resource conservation.'
                },{
                    title:'Design and Build',
                    content:'CCI incorporates design and construction into one cohesive process through a single point of accountability.'
                },{
                    title:'Construction / Construction Management',
                    content:'CCI provides construction service, all technical and administrative management service, effectively planning and managing the project from initial bidding stage through to the certificate of occupancy and final closeout.'
                },{
                    title:'Project  Operation and Program Management',
                    content:'CCI provides consulting for the capital process and comprehensive service including managing and coordinating all design, procurement and construction activities.'
                }
            ]
        },
        partner: {
            title: 'Our Clients'
        },
        contact: {
            company: 'Shanghai EZ Architecture Decoration Engineering Co., LTD',
            title: 'Contact Us',
            address: 'Address',
            addressValue: 'Unit 4C,Building 52, No. 461 HongCao Rd,Xu Hui District,Shanghai, P.R.China',
            post: 'Post Code',
            tel: 'Tel',
            mail: 'E-mail'
        },
        team: {
            title: 'Core Team Members',
            photoType1: 'Project Meeting',
            photoType2: 'Solution Discussion',
        },
        about:{
            content:'We are specialized in project design, construction, project management and other professional services for corporate headquarters, factories, R&D centers, training centers etc. It provides one-stop service to the clients—from site selection, feasibility studies, engineering design, project execution, project handover to facility management. Throughout the project life cycle, we not only provide clients with high-quality design and construction service, but also tailor-made the advice for clients, providing professional support to the greatest success of the project.'
        }
    }
}