import Vue from 'vue';
import VueRouter from 'vue-router';
import componentTransition from 'Components/component-transition';
import home from 'Components/home';
import about from 'Components/about';
import contact from 'Components/contact';
import projects from 'Components/projects';
import projectDetail from 'Components/project-detail';
import team from 'Components/team';
Vue.use(VueRouter);

const App = resolve => {
    require.ensure(['Components/app'], () => {
        resolve(require('Components/app'));
    });
};

const Home = resolve => {
    require.ensure(['Components/home'], () => {
        resolve(require('Components/home'));
    });
};

const About = resolve => {
    require.ensure(['Components/about'], () => {
        resolve(require('Components/about'));
    });
};
const Contact = resolve => {
    require.ensure(['Components/contact'], () => {
        resolve(require('Components/contact'));
    });
};

const ProjectDetail = resolve => {
    require.ensure(['Components/project-detail'], () => {
        resolve(require('Components/project-detail'));
    });
};
const Projects = resolve => {
    require.ensure(['Components/projects'], () => {
        resolve(require('Components/projects'));
    });
};
const Team = resolve => {
    require.ensure(['Components/team'], () => {
        resolve(require('Components/team'));
    });
};




const router = new VueRouter({
    mode: 'hash',
    routes: [{
        path: '/',
        component: componentTransition,
        children: [{
                path: '',
                component: Home
            },
            
            {
                path: '/about',
                component: About
            },
            {
                path: '/contact',
                component: Contact
            },
            {
                path: '/projects',
                component: Projects
            },
            {
                path: '/team',
                component: Team
            },{
                path: '/projectDetail',
                component: ProjectDetail
            }


        ]
    }],
    scrollBehavior(to, from, savedPosition) {
        return { x: 0, y: 0 }
    }
});

export default router