/* 引入操作路径模块和webpack */
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlwebpackPlugin = require('html-webpack-plugin');
const poststylus = require('poststylus');
const BabelEnginePlugin = require('babel-engine-plugin');
//const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
module.exports = {
    /* 输入文件 */
    entry: {
        app: './Public/Js/app.js'
    },
    output: {
        /* 输出目录，没有则新建 */
        path: path.resolve(__dirname, './Dist/'),
        /* 文件名 */
        filename: 'Js/[name].min.[hash].js'
    },
    module: {
        rules: [
            /* 用来解析vue后缀的文件 */
            {
                test: /\.vue$/,
                use: {
                    loader: "vue-loader",
                    options: {
                        loaders: {
                            css: ExtractTextPlugin.extract({
                                use: 'css-loader'
                            }),
                            stylus: ExtractTextPlugin.extract({
                                use: ["css-loader", "stylus-loader"]
                            })
                        }
                    }
                }
            },
            /* 用babel来解析js文件并把es6的语法转换成浏览器认识的语法 */
            {
                test: /\.js$/,
                loader: 'babel-loader',
                /* 排除模块安装目录的文件 */
                exclude: /node_modules/
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: "css-loader"
                })
            }, {
                test: /\.styl$/,
                use: ExtractTextPlugin.extract({
                    use: ["css-loader", "stylus-loader"]
                })
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                use: [{
                    loader: "url-loader",
                    options: {
                        limit: 102400,
                        outputPath:'../Images/',
                        name: '[name].[ext]' // 将图片都放入images文件夹下，[hash:7]防缓存
                    }
                }]
            },
        ]
    },
    resolve: {
        extensions: ['.js', '.vue', '.css', '.styl'],
        alias: {
            '$': path.resolve(__dirname, 'node_modules/jquery/dist/jquery.slim.min.js'),
            'jQuery': path.resolve(__dirname, 'node_modules/jquery/dist/jquery.slim.min.js'),
            'vue': path.resolve(__dirname, 'node_modules/vue/dist/vue.min.js'),
            'Components': path.resolve(__dirname, 'Components'),
            'Public': path.resolve(__dirname, 'Public')
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: '"production"'
            }
        }),
        new ExtractTextPlugin("Css/styles.[hash].css"),
        new BabelEnginePlugin({})
    ],
    devtool: 'cheap-module-source-map'
}