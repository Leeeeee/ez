/* 引入操作路径模块和webpack */
const path = require('path');
const webpack = require('webpack');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const HtmlwebpackPlugin = require('html-webpack-plugin');
const poststylus = require('poststylus');
const AsyncModulePlugin = require('async-module-loader/plugin');
module.exports = {
    /* 输入文件 */
    entry: { app: './Public/Js/app.js' },
    output: {
        /* 输出目录，没有则新建 */
        path: path.resolve(__dirname, './Dist/'),
        publicPath: 'http://localhost:8080/Dist/',
        /* 文件名 */
        filename: 'Js/[name].min.js'
    },
    module: {
        rules: [
            /* 用来解析vue后缀的文件 */
            {
                test: /\.vue$/,
                use: {
                    loader: "vue-loader",
                    options: {
                        loaders: {
                            css: ExtractTextPlugin.extract({
                                use: ['css-loader']
                            }),
                            stylus: ExtractTextPlugin.extract({
                                use: ["css-loader", "stylus-loader"]
                            })
                        }
                    }
                }
            },
            /* 用babel来解析js文件并把es6的语法转换成浏览器认识的语法 */
            {
                test: /\.js$/,
                loader: "babel-loader",
                query: { presets: ['es2015'] },
                exclude: /node_modules/
            }, {
                test: /\.css$/,
                use: ExtractTextPlugin.extract({
                    use: ["css-loader"]
                })
            }, {
                test: /\.styl$/,
                use: ExtractTextPlugin.extract({
                    use: ["css-loader", "stylus-loader"]
                })
            },
            {
                test: /\.(png|jpe?g|gif|svg)(\?.*)?$/,
                use: [{
                    loader: "url-loader",
                    options: {
                        limit: 100000,
                        name: 'Images/[name].[hash:7].[ext]' // 将图片都放入images文件夹下，[hash:7]防缓存
                    }
                }]
            },
            {
                test: /\.(woff2?|eot|ttf|otf)(\?.*)?$/,
                use: [{
                    loader: "url-loader",
                    options: {
                        limit: 10000,
                        name: 'Fonts/[name].[hash:7].[ext]' // 将字体放入fonts文件夹下
                    }
                }]
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.vue', '.css', '.styl'],
        alias: {
            '$': path.resolve(__dirname, 'node_modules/jquery/dist/jquery.js'),
            'jQuery': path.resolve(__dirname, 'node_modules/jquery/dist/jquery.js'),
            'vue': path.resolve(__dirname, 'node_modules/vue/dist/vue.js'),
            'Components': path.resolve(__dirname, 'Components'),
            'Public': path.resolve(__dirname, 'Public')
        }
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new ExtractTextPlugin("Css/styles.css"),
        new HtmlwebpackPlugin(),
        new AsyncModulePlugin()
    ],
    devtool: 'source-map'
}